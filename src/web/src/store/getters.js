const getters = {
  // 主界面
  sidebar: state => state.app.sidebar,
  userInfo: state => state.user.userInfo,
  pocketData: state => state.pocket.pocketData,
}

export default getters
