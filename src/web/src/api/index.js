import admin from './admin'
import category from './category'
import company from './company'
import department from './department'
import depot from './depot'
import material from './material'
import menu from './menu'
import role from './role'
import user from './user'

const api = {
  admin,
  category,
  company,
  department,
  depot,
  material,
  menu,
  role,
  user,
}

export default api;
