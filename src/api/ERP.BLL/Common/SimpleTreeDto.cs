﻿using ERP.Common.Model;

namespace ERP.BLL.Common
{
    public class SimpleTreeDto:SimpleTreeModel
    {
        public string Name { get; set; }

        public object Children { get; set; }
    }
}
