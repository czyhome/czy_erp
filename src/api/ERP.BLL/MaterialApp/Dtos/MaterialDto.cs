﻿using System;
using ERP.BLL.BaseApp.Dtos;
namespace ERP.BLL.MaterialApp.Dtos
{
    /// <summary>
    /// 产品映射实体
    /// </summary>
    public class MaterialDto : BaseDto
    {

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 制造商
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// 包装(KG/包)
        /// </summary>
        public double? Packing { get; set; }

        /// <summary>
        /// 安全存量(KG)
        /// </summary>
        public double? SafetyStock { get; set; }

        /// <summary>
        /// 型号
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string Standard { get; set; }

        /// <summary>
        /// 颜色
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 零售价
        /// </summary>
        public double? RetailPrice { get; set; }

        /// <summary>
        /// 最低售价
        /// </summary>
        public double? LowPrice { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 所属类别Id
        /// </summary>
        public Guid CategoryId { get; set; }
    }

}
