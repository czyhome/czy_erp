﻿using System;

namespace ERP.BLL.MaterialApp.Dtos
{
    public class DepotMaterialDto
    {
        public Guid MaterialId { get; set; }
        public Guid DepotId { get; set; }
    }
}
