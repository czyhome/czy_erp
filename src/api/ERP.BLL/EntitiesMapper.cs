﻿using AutoMapper;
using ERP.BLL.CategoryApp.Dtos;
using ERP.BLL.CompanyApp.Dtos;
using ERP.BLL.DepartmentApp.Dtos;
using ERP.BLL.DepotApp.Dtos;
using ERP.BLL.MaterialApp.Dtos;
using ERP.BLL.MenuApp.Dtos;
using ERP.BLL.RoleApp.Dtos;
using ERP.BLL.UserApp.Dtos;
using ERP.Common.Model;
using ERP.Model.Entities;

namespace ERP.BLL
{
    /// <summary>
    /// Entity与Dto映射
    /// </summary>
    public class EntitiesMapper
    {
        public static void Initialize()
        {

            Mapper.Initialize(cfg =>
            {
                #region 公司相关
                cfg.CreateMap<Company, CompanyDto>();
                cfg.CreateMap<CompanyDto, Company>();

                cfg.CreateMap<Department, DepartmentDto>();
                cfg.CreateMap<DepartmentDto, Department>();
                #endregion

                #region 用户相关
                cfg.CreateMap<Menu, MenuDto>();
                cfg.CreateMap<Menu, SimpleTreeModel>();
                cfg.CreateMap<MenuDto, Menu>();

                cfg.CreateMap<Role, RoleDto>();

                cfg.CreateMap<RoleDto, Role>();

                cfg.CreateMap<RoleMenu, RoleMenuDto>();
                cfg.CreateMap<RoleMenuDto, RoleMenu>();

                cfg.CreateMap<User, UserDto>()
                .ForMember(t => t.UserRoleDtos, opt =>
                {
                    opt.MapFrom(s => s.UserRoles);
                });

                cfg.CreateMap<UserDto, User>();
                cfg.CreateMap<UserDto, UserInfo>();

                cfg.CreateMap<UserRole, UserRoleDto>();
                cfg.CreateMap<UserRoleDto, UserRole>();
                #endregion


                cfg.CreateMap<Depot, DepotDto>();
                cfg.CreateMap<DepotDto, Depot>();

                cfg.CreateMap<Material, MaterialDto>();
                cfg.CreateMap<MaterialDto, Material>();

                cfg.CreateMap<Category, CategoryDto>();
                cfg.CreateMap<CategoryDto, Category>();

                cfg.CreateMap<DepotMaterial, DepotMaterialDto>();
                cfg.CreateMap<DepotMaterialDto, DepotMaterial>();

                cfg.CreateMap<Category, SimpleTreeModel>();
            });
        }
    }
}
