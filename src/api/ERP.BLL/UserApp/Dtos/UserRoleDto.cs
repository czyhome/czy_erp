﻿using System;

namespace ERP.BLL.UserApp.Dtos
{
    public class UserRoleDto
    {
        public Guid UserId { get; set; }

        public Guid RoleId { get; set; }

    }
}
