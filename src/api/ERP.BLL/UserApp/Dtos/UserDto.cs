﻿using System;
using System.Collections.Generic;
using ERP.BLL.BaseApp.Dtos;

namespace ERP.BLL.UserApp.Dtos
{
    /// <summary>
    /// 用户映射实体
    /// </summary>
    public class UserDto : BaseDto
    {

        /// <summary>
        /// 用户账号
        /// </summary>
        public string LoginName { get; set; }

        /// <summary>
        /// 用户密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 电话号码
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 是否部长
        /// </summary>
        public bool IsHeader { get; set; }

        /// <summary>
        /// 所属部门Id
        /// </summary>
        public Guid DepartmentId { get; set; }

        public List<UserRoleDto> UserRoleDtos { get; set; }

    }
}

