﻿namespace ERP.BLL.Model
{
    public class PagableCriteria
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
