﻿using System;

namespace ERP.BLL.Model
{
    public class SearchMaterialModel : PagableCriteria
    {
        public Guid DepotId { get; set; }

        public Guid CategoryId { get; set; }
    }
}
