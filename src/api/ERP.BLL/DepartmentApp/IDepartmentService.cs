﻿using System;
using System.Collections.Generic;
using ERP.BLL.BaseApp;
using ERP.BLL.DepartmentApp.Dtos;

namespace ERP.BLL.DepartmentApp
{
    public interface IDepartmentService : IServiceBase
    {
        List<DepartmentDto> GetDepsByComId(Guid companyId);

        /// <summary>
        /// 添加部门
        /// </summary>
        /// <param name="dto">部门实体</param>
        /// <returns>默认可以添加=0,重名=1</returns>
        int Insert(DepartmentDto dto);

        List<DepartmentDto> GetDepartmentList();
        /// <summary>
        /// 根据部门Id修改信息
        /// </summary>
        /// <param name="dto">部门实体</param>
        /// <returns>bool</returns>
        bool Update(DepartmentDto dto);

        /// <summary>
        /// 根据Id删除部门
        /// </summary>
        /// <param name="id">Id</param>
        void Delete(Guid id);
    }
}
