﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ERP.BLL.DepartmentApp.Dtos;
using ERP.IDAL.IRepositories.basic;
using ERP.Model.Entities;

namespace ERP.BLL.DepartmentApp
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IDepartmentRepository _repository;

        public DepartmentService(IDepartmentRepository repository)
        {
            _repository = repository;
        }

        public List<DepartmentDto> GetDepsByComId(Guid companyId)
        {
            return Mapper.Map<List<DepartmentDto>>(_repository.GetAllListAsync(r => r.CompanyId == companyId).Result);
        }

        public int Insert(DepartmentDto dto)
        {
            var dep = _repository.FirstOrDefaultAsync(t => t.Name == dto.Name).Result;
            if (dep != null)
            {
                return 1;
            }

            _repository.InsertAsync(Mapper.Map<Department>(dto));
            return 0;
        }

        public bool Update(DepartmentDto dto)
        {
            return _repository.UpdateAsync(Mapper.Map<Department>(dto)).Result != null;
        }

        public void Delete(Guid id)
        {
            _repository.Delete(id);
        }

        public List<DepartmentDto> GetDepartmentList()
        {
            return Mapper.Map<List<DepartmentDto>>(_repository.GetAllListAsync().Result);
        }
    }
}