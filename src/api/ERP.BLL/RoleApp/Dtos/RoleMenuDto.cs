﻿using System;

namespace ERP.BLL.RoleApp.Dtos
{
    public class RoleMenuDto
    {
        public Guid RoleId { get; set; }

        public Guid MenuId { get; set; }
    }
}
