﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ERP.BLL.RoleApp.Dtos;
using ERP.Common.Extensions;
using ERP.Common.Model;
using ERP.IDAL.IRepositories.basic;
using ERP.Model.Entities;

namespace ERP.BLL.RoleApp
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IRoleMenuRepository _roleMenuRepository;
        private readonly IMenuRepository _menuRepository;

        public RoleService(IRoleRepository roleRepository, IRoleMenuRepository roleMenuRepository,
            IMenuRepository menuRepository)
        {
            _roleRepository = roleRepository;
            _roleMenuRepository = roleMenuRepository;
            _menuRepository = menuRepository;
        }

        public List<RoleDto> GetRoleList()
        {
            return Mapper.Map<List<RoleDto>>(_roleRepository.GetAllListAsync().Result);
        }

        public PagedResultModel<RoleDto> GetRolePageList(int pageIndex, int pageSize)
        {
            return Mapper.Map<List<RoleDto>>(_roleRepository.LoadPageList(pageIndex, pageSize, out var rowCount,
                t => true,
                t => t.Id, false)).ApplyToPage(pageIndex, pageSize, rowCount);
        }

        public RoleDto GetRole(Guid id)
        {
            throw new System.NotImplementedException();
        }

        public int Insert(RoleDto dto)
        {
            var role = _roleRepository.FirstOrDefaultAsync(t => t.Name == dto.Name).Result;
            if (role != null)
            {
                return 1;
            }
            else
            {
                _roleRepository.Insert(Mapper.Map<Role>(dto));
                return 0;
            }
        }

        public bool Update(RoleDto dto)
        {
            var role = _roleRepository.UpdateAsync(Mapper.Map<Role>(dto));
            return role != null;
        }

        public void Delete(Guid id)
        {
            _roleRepository.Delete(id);
        }

        public List<Guid> GetMenusByRole(Guid roleId)
        {
            return _roleMenuRepository.GetAllIncluding(t => t.Menu).Where(t => t.RoleId == roleId && t.Menu.IsMenu)
                .Select(t => t.MenuId).ToList();
        }

        public List<Guid> GetActionsByRole(Guid roleId)
        {
            return _roleMenuRepository.GetAllIncluding(t => t.Menu).Where(t => t.RoleId == roleId && !t.Menu.IsMenu)
                .Select(t => t.MenuId).ToList();
        }

        public bool UpdateRoleMenu(Guid roleId, List<RoleMenuDto> roleMenus)
        {
            var oldRoleMenus = _roleMenuRepository.GetAllIncluding(t => t.Menu)
                .Where(t => t.RoleId == roleId && t.Menu.IsMenu).ToList();
            oldRoleMenus.ForEach(t => _roleMenuRepository.Delete(t));
            roleMenus.ForEach(t => _roleMenuRepository.InsertAsync(Mapper.Map<RoleMenu>(t)));
            return true;
        }

        public bool UpdateRoleAction(Guid roleId, List<RoleMenuDto> roleActions)
        {
            var oldRoleActions = _roleMenuRepository.GetAllIncluding(t => t.Menu)
                .Where(t => t.RoleId == roleId && !t.Menu.IsMenu).ToList();
            oldRoleActions.ForEach(t => _roleMenuRepository.Delete(t));
            roleActions.ForEach(t => _roleMenuRepository.InsertAsync(Mapper.Map<RoleMenu>(t)));
            return true;
        }
    }
}