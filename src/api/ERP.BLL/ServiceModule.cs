﻿using Autofac;
using ERP.BLL.BaseApp;

namespace ERP.BLL
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly)
                .Where(t => t.IsAssignableTo<IServiceBase>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
