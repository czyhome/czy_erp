﻿using ERP.BLL.BaseApp.Dtos;

namespace ERP.BLL.CompanyApp.Dtos
{
    /// <summary>
    /// 公司映射实体
    /// </summary>
    public class CompanyDto : BaseDto
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司联系人
        /// </summary>
        public string ContactPerson { get; set; }

        /// <summary>
        /// 公司地址
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// 公司电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        ///公司传真
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// 公司邮编
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// 公司说明
        /// </summary>
        public string Comment { get; set; }
    }
}
