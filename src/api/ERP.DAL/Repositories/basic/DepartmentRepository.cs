﻿using ERP.EFCore;
using ERP.IDAL.IRepositories.basic;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
    public class DepartmentRepository : RepositoryBase<Department>, IDepartmentRepository
    {
        public DepartmentRepository(DataContext dbContext) : base(dbContext)
        {

        }
    }
}
