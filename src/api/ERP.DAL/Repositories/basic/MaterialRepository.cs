﻿using ERP.IDAL.IRepositories.basic;
using ERP.EFCore;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
     public class MaterialRepository : RepositoryBase<Material>,IMaterialRepository
    {   
        public MaterialRepository(DataContext dbContext) : base(dbContext)
        {

        }
    }
}
