﻿using ERP.EFCore;
using ERP.IDAL.IRepositories.basic;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
    public class CompanyRepository : RepositoryBase<Company>, ICompanyRepository
    {
        public CompanyRepository(DataContext dbContext) : base(dbContext)
        {

        }
    }
}
