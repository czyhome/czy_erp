﻿using ERP.IDAL.IRepositories.basic;
using ERP.EFCore;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
     public class CategoryRepository : RepositoryBase<Category>,ICategoryRepository
    {   
        public CategoryRepository(DataContext dbContext) : base(dbContext)
        {

        }
    }
}
