﻿using ERP.EFCore;
using ERP.IDAL.IRepositories.basic;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(DataContext dbContext) : base(dbContext)
        {


        }
    }
}
