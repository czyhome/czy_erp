﻿using ERP.Core.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace ERP.EFCore
{
    public class DbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            var configuration = AppConfiguration.Get(WebContentDirectoryFinder.CalculateContentRootFolder());
            optionsBuilder.UseMySql(configuration.GetConnectionString("MySqlConnection"));
            return new DataContext(optionsBuilder.Options);

        }
    }
}
