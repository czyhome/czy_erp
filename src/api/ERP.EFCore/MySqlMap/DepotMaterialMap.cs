﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class DepotMaterialMap : IBaseEntityMap<DepotMaterial>
    {
        public void Configure(EntityTypeBuilder<DepotMaterial> builder)
        {
            builder.ToTable("erp_depot_material");
            builder.HasKey(t => t.Id);

            builder.HasOne(t => t.Depot).WithMany(t => t.DepotMaterials)
                .HasForeignKey(fk => fk.DepotId);
            builder.HasOne(t => t.Material).WithMany(t => t.DepotMaterials)
                .HasForeignKey(fk => fk.MaterialId);

            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");
        }
    }
}
