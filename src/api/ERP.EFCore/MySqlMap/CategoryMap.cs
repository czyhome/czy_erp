﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class CategoryMap : IBaseEntityMap<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("erp_category");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Name).HasMaxLength(50);
            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");


        }
    }
}
