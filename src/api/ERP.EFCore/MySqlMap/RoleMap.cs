﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class RoleMap : IBaseEntityMap<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable("erp_role");
            builder.HasKey(r => r.Id);
            builder.Property(r => r.Name).HasMaxLength(50);
            builder.Property(r => r.Comment).HasMaxLength(100);
            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");
        }
    }
}
