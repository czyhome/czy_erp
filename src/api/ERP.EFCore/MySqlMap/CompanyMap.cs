﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class SystemConfigMap : IBaseEntityMap<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.ToTable("erp_company");
            builder.HasKey(r => r.Id);
            builder.Property(r => r.Name).HasMaxLength(50);
            builder.Property(r => r.ContactPerson).HasMaxLength(20);
            builder.Property(r => r.Location).HasMaxLength(100);
            builder.Property(r => r.Phone).HasMaxLength(20);
            builder.Property(r => r.Fax).HasMaxLength(20);
            builder.Property(r => r.Postcode).HasMaxLength(20);
            builder.Property(r => r.Comment).HasMaxLength(100);
            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");
        }
    }
}
