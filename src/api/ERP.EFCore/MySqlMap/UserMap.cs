﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class UserMap : IBaseEntityMap<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("erp_user");
            builder.HasKey(r => r.Id);
            builder.Property(r => r.LoginName).HasMaxLength(50).IsRequired();
            builder.Property(r => r.UserName).HasMaxLength(50).IsRequired();
            builder.Property(r => r.Password).HasMaxLength(100);
            builder.Property(r => r.IsHeader).HasDefaultValue(0);
            builder.Property(r => r.Email).HasMaxLength(50);
            builder.Property(r => r.Phone).HasMaxLength(20);

            builder.HasOne(r => r.Department).WithMany(t => t.Users)
                .HasForeignKey(fk => fk.DepartmentId);
            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");
        }
    }
}
