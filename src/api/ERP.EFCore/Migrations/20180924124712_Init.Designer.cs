﻿// <auto-generated />
using System;
using ERP.EFCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ERP.EFCore.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20180924124712_Init")]
    partial class Init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.3-rtm-32065")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("ERP.Model.Entities.Category", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<short?>("Level");

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<Guid>("ParentId");

                    b.HasKey("Id");

                    b.ToTable("erp_category");
                });

            modelBuilder.Entity("ERP.Model.Entities.Company", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Comment")
                        .HasMaxLength(100);

                    b.Property<string>("ContactPerson")
                        .HasMaxLength(20);

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<string>("Fax")
                        .HasMaxLength(20);

                    b.Property<string>("Location")
                        .HasMaxLength(100);

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<string>("Phone")
                        .HasMaxLength(20);

                    b.Property<string>("Postcode")
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.ToTable("erp_company");
                });

            modelBuilder.Entity("ERP.Model.Entities.Department", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Comment")
                        .HasMaxLength(100);

                    b.Property<Guid>("CompanyId");

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<Guid>("ParentId");

                    b.Property<string>("Phone")
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("erp_department");
                });

            modelBuilder.Entity("ERP.Model.Entities.Depot", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Address")
                        .HasMaxLength(50);

                    b.Property<string>("Comment")
                        .HasMaxLength(100);

                    b.Property<double?>("DepotCharge");

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<int?>("Sort");

                    b.Property<double?>("Truckage");

                    b.Property<int?>("Type");

                    b.HasKey("Id");

                    b.ToTable("erp_depot");
                });

            modelBuilder.Entity("ERP.Model.Entities.DepotMaterial", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<Guid>("DepotId");

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<Guid>("MaterialId");

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.HasKey("Id");

                    b.HasIndex("DepotId");

                    b.HasIndex("MaterialId");

                    b.ToTable("erp_depot_material");
                });

            modelBuilder.Entity("ERP.Model.Entities.Material", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<Guid>("CategoryId");

                    b.Property<string>("Color")
                        .HasMaxLength(20);

                    b.Property<string>("Comment")
                        .HasMaxLength(100);

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<double?>("LowPrice");

                    b.Property<string>("Manufacturer")
                        .HasMaxLength(50);

                    b.Property<string>("Model")
                        .HasMaxLength(20);

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<double?>("Packing");

                    b.Property<double?>("RetailPrice");

                    b.Property<double?>("SafetyStock");

                    b.Property<string>("Standard")
                        .HasMaxLength(20);

                    b.Property<string>("Unit")
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.ToTable("erp_material");
                });

            modelBuilder.Entity("ERP.Model.Entities.Menu", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Comment")
                        .HasMaxLength(100);

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<string>("Icon")
                        .HasMaxLength(100);

                    b.Property<bool>("IsMenu")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<Guid>("ParentId");

                    b.Property<int>("Sort")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(0);

                    b.Property<string>("Url")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("erp_menu");
                });

            modelBuilder.Entity("ERP.Model.Entities.Role", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Comment")
                        .HasMaxLength(100);

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("erp_role");
                });

            modelBuilder.Entity("ERP.Model.Entities.RoleMenu", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<Guid>("MenuId");

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<Guid>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("MenuId");

                    b.HasIndex("RoleId");

                    b.ToTable("erp_role_menu");
                });

            modelBuilder.Entity("ERP.Model.Entities.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<Guid>("DepartmentId");

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<bool>("IsHeader")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<string>("LoginName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<string>("Password")
                        .HasMaxLength(100);

                    b.Property<string>("Phone")
                        .HasMaxLength(20);

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.ToTable("erp_user");
                });

            modelBuilder.Entity("ERP.Model.Entities.UserRole", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedTime")
                        .HasColumnType("datetime");

                    b.Property<bool>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime");

                    b.Property<Guid>("RoleId");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("erp_user_role");
                });

            modelBuilder.Entity("ERP.Model.Entities.Department", b =>
                {
                    b.HasOne("ERP.Model.Entities.Company", "Company")
                        .WithMany("Departments")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ERP.Model.Entities.DepotMaterial", b =>
                {
                    b.HasOne("ERP.Model.Entities.Depot", "Depot")
                        .WithMany("DepotMaterials")
                        .HasForeignKey("DepotId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ERP.Model.Entities.Material", "Material")
                        .WithMany("DepotMaterials")
                        .HasForeignKey("MaterialId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ERP.Model.Entities.Material", b =>
                {
                    b.HasOne("ERP.Model.Entities.Category", "Category")
                        .WithMany("Materials")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ERP.Model.Entities.RoleMenu", b =>
                {
                    b.HasOne("ERP.Model.Entities.Menu", "Menu")
                        .WithMany("RoleMenus")
                        .HasForeignKey("MenuId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ERP.Model.Entities.Role", "Role")
                        .WithMany("RoleMenus")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ERP.Model.Entities.User", b =>
                {
                    b.HasOne("ERP.Model.Entities.Department", "Department")
                        .WithMany("Users")
                        .HasForeignKey("DepartmentId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ERP.Model.Entities.UserRole", b =>
                {
                    b.HasOne("ERP.Model.Entities.Role", "Role")
                        .WithMany("UserRoles")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ERP.Model.Entities.User", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
