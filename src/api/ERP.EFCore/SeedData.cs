﻿using System;
using System.Linq;
using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ERP.EFCore
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new DataContext(serviceProvider.GetRequiredService<DbContextOptions<DataContext>>()))
            {
                if (context.Set<User>().Any())
                {
                    return;
                }

                var company = new Company
                {
                    Name = "思梦科技有限公司",
                    ContactPerson = "陈昭宇",
                    Fax = "0451 - 4323424",
                    Location = "上海市",
                    Phone = "15145033859",
                    Postcode = "150637",
                    Comment = "无",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                var department = new Department
                {
                    Name = "沈阳京东世纪贸易有限公司",
                    ParentId = Guid.Empty,
                    Phone = "083958952",
                    Company = company,
                    Comment = "东北京东物流总中心",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                var user = new User
                {
                    LoginName = "admin",
                    Password = "123456",
                    UserName = "陈昭宇",
                    Email = "805899926@qq.com",
                    Phone = "15145033859",
                    Department = department,
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                context.Set<Company>().Add(company);
                context.Set<Department>().Add(department);
                context.Set<User>().Add(user);

                var sysManage = new Menu
                {
                    ParentId = Guid.Empty,
                    Icon = "erp-icon-system",
                    Name = "系统管理",
                    Sort = 1,
                    Url = "#",
                    IsMenu = true,
                    Comment = "用于系统管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                var sysEntity = context.Set<Menu>().Add(sysManage).Entity;

                var userManage = new Menu
                {
                    ParentId = sysEntity.Id,
                    Icon = "erp-icon-usermanage",
                    Name = "用户管理",
                    IsMenu = true,
                    Sort = 1,
                    Url = "User",
                    Comment = "用于用户管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };

                var roleManage = new Menu
                {
                    ParentId = sysEntity.Id,
                    Icon = "erp-icon-role",
                    Name = "角色管理",
                    IsMenu = true,
                    Sort = 3,
                    Url = "Role",
                    Comment = "用于角色管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };

                var menuManage = new Menu
                {
                    ParentId = sysEntity.Id,
                    Icon = "erp-icon-menu",
                    Name = "菜单管理",
                    IsMenu = true,
                    Sort = 4,
                    Url = "Menu",
                    Comment = "用于菜单管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };

                context.Set<Menu>().Add(userManage);
                context.Set<Menu>().Add(roleManage);
                context.Set<Menu>().Add(menuManage);

                var organzeManage = new Menu
                {
                    ParentId = sysEntity.Id,
                    Icon = "erp-icon-organization",
                    Name = "组织管理",
                    IsMenu = true,
                    Sort = 5,
                    Url = "#",
                    Comment = "用于组织管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                var organzeEntity = context.Set<Menu>().Add(organzeManage).Entity;

                var departmentManage = new Menu
                {
                    ParentId = organzeEntity.Id,
                    Icon = "erp-icon-department",
                    Name = "部门管理",
                    IsMenu = true,
                    Sort = 0,
                    Url = "Department",
                    Comment = "用于部门管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                var companyManage = new Menu
                {
                    ParentId = organzeEntity.Id,
                    Icon = "erp-icon-company",
                    Name = "公司管理",
                    IsMenu = true,
                    Sort = 0,
                    Url = "Company",
                    Comment = "用于公司管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                context.Set<Menu>().Add(departmentManage);
                context.Set<Menu>().Add(companyManage);

                var storageManage = new Menu
                {
                    ParentId = Guid.Empty,
                    Icon = "erp-icon-storage",
                    Name = "仓储管理",
                    IsMenu = true,
                    Sort = 0,
                    Url = "#",
                    Comment = "用于仓储管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                var storageEntity = context.Set<Menu>().Add(storageManage).Entity;
                var warehouManage = new Menu
                {
                    ParentId = storageEntity.Id,
                    Icon = "erp-icon-warehouse",
                    Name = "仓库管理",
                    IsMenu = true,
                    Sort = 0,
                    Url = "Depot",
                    Comment = "用于仓库管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                var materialManage = new Menu
                {
                    ParentId = storageEntity.Id,
                    Icon = "erp-icon-material",
                    Name = "商品管理",
                    IsMenu = true,
                    Sort = 0,
                    Url = "Material",
                    Comment = "用于商品管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                var materialAnalysis = new Menu
                {
                    ParentId = storageEntity.Id,
                    Icon = "erp-icon-material",
                    Name = "商品统计",
                    IsMenu = true,
                    Sort = 0,
                    Url = "MaterialAnalysis",
                    Comment = "用于商品统计的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                var categoryManage = new Menu
                {
                    ParentId = storageEntity.Id,
                    Icon = "erp-icon-classify",
                    Name = "分类管理",
                    IsMenu = true,
                    Sort = 0,
                    Url = "Category",
                    Comment = "用于商品分类管理的菜单",
                    AddedTime = DateTime.Now,
                    ModifiedTime = DateTime.Now
                };
                context.Set<Menu>().Add(warehouManage);
                context.Set<Menu>().Add(materialManage);
                context.Set<Menu>().Add(materialAnalysis);
                context.Set<Menu>().Add(categoryManage);

                context.SaveChanges();
            }
        }
    }
}
