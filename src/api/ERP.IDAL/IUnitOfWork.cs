﻿using System.Data.Common;

namespace ERP.IDAL
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// 开启事务
        /// </summary>
        /// <returns></returns>
        DbTransaction BeginTransaction();

        /// <summary>
        /// 提交事务
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// 回滚事务
        /// </summary>
        void RollbackTransaction();
    }
}
