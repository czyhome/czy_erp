﻿using ERP.Model.Entities;

namespace ERP.IDAL.IRepositories.basic
{
     public interface ICategoryRepository : IRepositoryBase<Category>
    {   
        
    }
}
