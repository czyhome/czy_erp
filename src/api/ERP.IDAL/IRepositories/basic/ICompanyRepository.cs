﻿using ERP.Model.Entities;

namespace ERP.IDAL.IRepositories.basic
{
     public interface ICompanyRepository : IRepositoryBase<Company>
    {   
        
    }
}
