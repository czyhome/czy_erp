﻿using ERP.Model.Entities;

namespace ERP.IDAL.IRepositories.basic
{
     public interface IDepotMaterialRepository : IRepositoryBase<DepotMaterial>
    {   
        
    }
}
