﻿using System.Collections.Generic;

namespace ERP.Common.Model
{
    public class PagedResultModel<T>
    {
        public IList<T> List;
        public PageWithTotalModel Page;

        public PagedResultModel(IList<T> list, PageWithTotalModel page)
        {
            List = list;
            Page = page;
        }
    }
}
