﻿namespace ERP.Common.Model
{
    public class SimpleItemModel
    {
        public string Value
        {
            get;
            set;
        }

        public string Label
        {
            get;
            set;
        }

        public string ParentId
        {
            get;
            set;
        }
    }
}
