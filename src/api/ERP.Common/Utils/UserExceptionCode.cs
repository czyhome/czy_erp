﻿namespace ERP.Common.Utils
{
    /// <summary>
    /// 用户错误代码定义
    /// </summary>
    public class UserExceptionCode : IExceptionCodeConstants
    {

        /// <summary>
        /// 用户不存在
        /// </summary>
        public const int USER_NOT_EXIST = 1;
        /// <summary>
        /// 用户密码错误
        /// </summary>
        public const int USER_PASSWORD_ERROR = 2;
        /// <summary>
        /// 被加入黑名单
        /// </summary>
        public const int BLACK_USER = 3;
        /// <summary>
        /// 可以登录
        /// </summary>
        public const int USER_CONDITION_FIT = 4;
        /// <summary>
        /// 访问数据库异常
        /// </summary>
        public const int USER_ACCESS_EXCEPTION = 5;
    }

}
