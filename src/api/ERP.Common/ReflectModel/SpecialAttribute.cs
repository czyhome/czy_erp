﻿namespace ERP.Common.ReflectModel
{
    public class SpecialAttribute
    {

        /// <summary>
        /// 特性类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 类型值
        /// </summary>
        public string Value { get; set; }
    }
}
