﻿using System.Collections.Generic;

namespace ERP.Common.ReflectModel
{
    public class MethodModel
    {
        /// <summary>
        /// 方法名称
        /// </summary>
        public string MethodName { get; set; }

        /// <summary>
        /// 方法参数集合
        /// </summary>
        public List<FormalParameter> MethodParams { get; set; }
    }
}