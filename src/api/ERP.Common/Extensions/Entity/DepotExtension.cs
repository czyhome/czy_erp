﻿using System.Collections.Generic;
using System.Linq;
using ERP.Common.Model;
using ERP.Model.Entities;

namespace ERP.Common.Extensions.Entity
{
    public static class DepotExtension
    {
        public static IList<SimpleItemModel> ConvertToSimple(this IEnumerable<Depot> depots)
        {
            return depots?.Select(s => new SimpleItemModel
            {
                Label = s.Name,
                Value = s.Id.ToString(),
            }).ToList();
        }
    }
}
