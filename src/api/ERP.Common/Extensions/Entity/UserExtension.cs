﻿using System.Collections.Generic;
using System.Linq;
using ERP.Common.Model;
using ERP.Model.Entities;

namespace ERP.Common.Extensions.Entity
{
    public static class UserExtension
    {
        public static IList<SimpleItemModel> ConvertToSimple(this IEnumerable<User> users)
        {
            return users?.Select(s => new SimpleItemModel
            {
                Label = s.UserName,
                Value = s.Id.ToString()
            }).ToList();
        }
    }
}
