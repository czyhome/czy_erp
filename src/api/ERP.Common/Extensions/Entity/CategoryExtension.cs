﻿using System.Collections.Generic;
using System.Linq;
using ERP.Common.Model;
using ERP.Model.Entities;

namespace ERP.Common.Extensions.Entity
{
    public static class CategoryExtension
    {
        public static IList<SimpleItemModel> ConvertToSimple(this IEnumerable<Category> categories)
        {
            return categories?.Select(s => new SimpleItemModel
            {
                Label = s.Name,
                Value = s.Id.ToString(),
                ParentId = s.ParentId.ToString()
            }).ToList();
        }
    }
}
