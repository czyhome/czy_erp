﻿using System.Collections.Generic;
using System.Linq;
using ERP.Common.Model;
using ERP.Model.Entities;

namespace ERP.Common.Extensions.Entity
{
    public static class DepartmentExtension
    {
        public static IList<SimpleItemModel> ConvertToSimple(this IEnumerable<Department> departments)
        {
            return departments?.Select(s => new SimpleItemModel
            {
                Label = s.Name,
                Value = s.Id.ToString(),
            }).ToList();
        }
    }
}
