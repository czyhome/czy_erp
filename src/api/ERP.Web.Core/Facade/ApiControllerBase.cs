﻿using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Core.Facade
{
    public class ApiControllerBase : Controller
    {
        protected internal virtual string OutputDataDefaultFieldName => "data";
        protected internal virtual string OutputPocketDefaultFieldName => "pocket";
    }
}
