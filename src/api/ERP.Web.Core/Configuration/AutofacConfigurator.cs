﻿using System;
using Autofac;
using Autofac.Configuration;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ERP.Web.Core.Configuration
{
    public static class AutofacConfigurator
    {
        public static IContainer ApplicationContainer { get; set; }

        public static IServiceProvider Configure(IServiceCollection services, IConfiguration configuration)
        {
            var builder = new ContainerBuilder();
            builder.Populate(services);
            var module = new ConfigurationModule(configuration);
            //注入数据层和业务层的实例
            builder.RegisterModule(module);
            ApplicationContainer = builder.Build();
            return new AutofacServiceProvider(ApplicationContainer);
        }
    }
}
