﻿using System.Collections.Generic;

namespace ERP.Model.Entities
{
    /// <summary>
    /// 角色实体
    /// </summary>
    public class Role : BaseEntity
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 角色说明
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 用户角色实体集合
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }

        /// <summary>
        /// 角色菜单实体集合
        /// </summary>
        public virtual ICollection<RoleMenu> RoleMenus { get; set; }

    }
}
