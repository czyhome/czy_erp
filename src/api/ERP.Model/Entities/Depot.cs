﻿using System.Collections.Generic;

namespace ERP.Model.Entities
{
    /// <summary>
    /// 仓库实体
    /// </summary>
    public class Depot : BaseEntity
    {
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 仓库地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 仓储费
        /// </summary>
        public double? DepotCharge { get; set; }

        /// <summary>
        /// 搬运费
        /// </summary>
        public double? Truckage { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public int? Type { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int? Sort { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 仓库产品实体集合
        /// </summary>
        public virtual ICollection<DepotMaterial> DepotMaterials { get; set; }

    }

}
