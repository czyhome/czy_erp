﻿using System;
using System.Collections.Generic;

namespace ERP.Model.Entities
{
    /// <summary>
    /// 部门实体
    /// </summary>
    public class Department : BaseEntity
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 上级部门
        /// </summary>
        public Guid ParentId { get; set; }
        
        /// <summary>
        /// 部门电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 部门说明
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 所属公司Id
        /// </summary>
        public Guid CompanyId { get; set; }

        /// <summary>
        /// 所属公司实体
        /// </summary>
        public virtual Company Company { get; set; }

        /// <summary>
        /// 用户实体集合
        /// </summary>
        public virtual ICollection<User> Users { get; set; }
    }
}
