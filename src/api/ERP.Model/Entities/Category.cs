﻿using System;
using System.Collections.Generic;

namespace ERP.Model.Entities
{
    /// <summary>
    /// 商品类别实体
    /// </summary>
    public class Category : BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 等级
        /// </summary>
        public short? Level { get; set; }

        /// <summary>
        /// 上级Id
        /// </summary>
        public Guid ParentId { get; set; }

        /// <summary>
        /// 产品实体集合
        /// </summary>
        public virtual ICollection<Material> Materials { get; set; }

    }
}
