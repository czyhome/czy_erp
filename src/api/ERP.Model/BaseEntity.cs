﻿using System;

namespace ERP.Model
{
    /// <summary>
    /// 泛型实体基类
    /// </summary>
    /// <typeparam name="TPrimaryKey">主键类型</typeparam>
    public class BaseEntity<TPrimaryKey>
    {
        /// <summary>
        /// 主键
        /// </summary>
        public TPrimaryKey Id { get; set; }
        public DateTime AddedTime { get; set; }
        public DateTime ModifiedTime { get; set; }
        public bool Enabled { get; set; }
    }

    /// <summary>
    /// 定义默认主键类型为long的实体基类
    /// </summary>
    public abstract class BaseEntity : BaseEntity<Guid> 
    {

    }
}
