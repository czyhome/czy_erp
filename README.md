#### 项目介绍
基于Asp.Net Core和Vue前后分离的ERP管理系统

目前已有功能:

 **权限管理**   **仓库管理**  **分类管理**  **商品管理** 

#### 软件架构
软件架构说明

开发环境:Windows10,Ubuntu 18.0.4

开发工具:Vistual Studio 2017,WebStorm 2018.2,Postman,Putty,WinScp,PyCharm,Navicat Premium

后端技术:Net Core SDK 2.1,EF Core 2.1

前端技术:Vue2.5.2,Vue-Router,Vuex

数据库:MySql 5.7,数据库编码:UTF-8

环境部署:阿里云ECS服务器(Ubuntu 18.0.4)